import "./App.css";
import React from "react";

function App() {
  const [name, setName] = React.useState("");
  const [data, setData] = React.useState([]);
  const [error, setError] = React.useState(null);
  const handlesearch = () => {
    // `https://www.rappad.co/rhymes-with/${name}`
    fetch(`https://api.datamuse.com/words?rel_rhy=${name}`)
      .then((response) => response.json())
      .then((data) => {
        data.length === 0 ? setError("data not available") : setData(data);
      })
      .catch((error) => console.log(error));
  };
  return (
    <div className="App">
      <label>
        Value :
        <input
          type="text"
          onChange={(e) => setName(e.target.value)}
          value={name}
        ></input>
        <button type="submit" onClick={handlesearch}>
          Search
        </button>
        {!error ? (
          data.map((i, index) => {
            return <p key={index}>{i.word}</p>;
          })
        ) : (
          <p>{error}</p>
        )}
      </label>
    </div>
  );
}

export default App;
